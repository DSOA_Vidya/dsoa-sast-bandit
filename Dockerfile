FROM python:3

COPY ./vulpy /vulpy

WORKDIR /

RUN pip3 install --user -r /vulpy/requirements.txt
RUN python3 /vulpy/web_app_one/db_init.py

ENTRYPOINT ["python3"]

CMD ["/vulpy/web_app_one/vulpy.py"]